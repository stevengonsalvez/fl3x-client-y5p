<?xml version="1.0" encoding="ISO-8859-1" standalone="no"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:user="urn:user" xmlns:var="urn:var" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" exclude-result-prefixes="user xs fn  user" version="2.0">
	<xsl:output encoding="UTF-8" indent="yes" method="xml" omit-xml-declaration="yes"/>
	<xsl:template match="/dataPack">
		<OrderExtract>
			<xsl:for-each select="/dataPack/order">
				<orderData>
					<language>en_GB</language>
					<channel>
						<xsl:value-of select="./order-header/order-type"/>
					</channel>
					<merchant>
						<type>logistics</type>
						<name>sekologistics</name>
						<merchantId>M01231</merchantId>
						<channel>api</channel>
					</merchant>
					<seller>
						<type>organisation</type>
						<sellerId>S00011</sellerId>
						<name>Y5P</name>
					</seller>
					<customer>
						<type>new</type>
						<email>
							<xsl:value-of select="./order-header/customer/email"/>
						</email>
						<customerId>
							<xsl:value-of select="./order-header/customer/email"/>
						</customerId>
					</customer>
					<orderHeader>
						<type>customerorder</type>
						<orderId>
							<xsl:value-of select="./order-header/order-number"/>
						</orderId>
						<orderDate>
							<xsl:value-of select="./order-header/date"/>
						</orderDate>
						<userSourceData>
							<device>unknown</device>
							<ipAddress>unknown</ipAddress>
						</userSourceData>
						<giftDetails>
							<orderIsGift>false</orderIsGift>
						</giftDetails>
						<charges>
							<currency>GBP</currency>
							<netPrice>
								<xsl:value-of select="./order-detail/order-totals/subtotal"/>
							</netPrice>
							<grossPrice>
								<xsl:value-of select="./order-detail/order-totals/subtotal"/>
							</grossPrice>
							<effectivePrice>
								<xsl:value-of select="./order-detail/order-totals/grandtotal"/>
							</effectivePrice>
							<specialPackageCharge>0.00</specialPackageCharge>
							<adjustment>0.00</adjustment>
							<salesTax>0.00</salesTax>
							<netShippingCharge>0.00</netShippingCharge>
							<grossShippingCharge>0.00</grossShippingCharge>
							<shippingOfferAdjustment>0.00</shippingOfferAdjustment>
							<shippingTax>
								<xsl:value-of select="./order-detail/order-totals/shipping-tax"/>
							</shippingTax>
						</charges>
						<promotion>
							<sellerPromotionCode>NO-PROMOTION</sellerPromotionCode>
						</promotion>
						<status>
							<customerStatus>
								<xsl:value-of select="./order-header/status"/>
							</customerStatus>
							<warehouseStatus>pending</warehouseStatus>
							<apiNotes>empty</apiNotes>
							<userNotes>empty</userNotes>
							<callCenterNotes>empty</callCenterNotes>
							<orderCompleted>false</orderCompleted>
							<isCancelled>false</isCancelled>
							<lastUpdatedTimestamp>
								<xsl:value-of select="current-dateTime()"/>
							</lastUpdatedTimestamp>
						</status>
						<billingInfo>
							<address>
								<type>primary</type>
								<xsl:choose>
									<xsl:when test="./order-header/customer/billing/title">
										<title>
											<xsl:value-of select="./order-header/customer/billing/title"/>
										</title>
									</xsl:when>
									<xsl:otherwise>
										<title>Mr/Mrs</title>
									</xsl:otherwise>
								</xsl:choose>
								<firstName>
									<xsl:value-of select="substring-before(./order-header/customer/billing/name, ' ')"/>
								</firstName>
								<lastName>
									<xsl:value-of select="substring-after(./order-header/customer/billing/name, ' ')"/>
								</lastName>
								<company>
									<xsl:value-of select="./order-header/customer/billing/company"/>
								</company>
								<division>
									<xsl:value-of select="./order-header/customer/billing/division"/>
								</division>
								<addressLine1>
									<xsl:value-of select="./order-header/customer/billing/street"/>
								</addressLine1>
								<town>
									<xsl:value-of select="./order-header/customer/billing/street"/>
								</town>
								<country>UK</country>
								<postcode>
									<xsl:value-of select="./order-header/customer/billing/zip"/>
								</postcode>
								<email>
									<xsl:value-of select="./order-header/customer/email"/>
								</email>
								<phone>
									<xsl:value-of select="./order-header/customer/billing/phone"/>
								</phone>
							</address>
						</billingInfo>
						<shippingInfo>
							<deliveryInformation>
								<deliveryMethod>
									<xsl:value-of select="./order-header/payment-type"/>
								</deliveryMethod>
							</deliveryInformation>
							<address>
								<type>home</type>
								<xsl:choose>
									<xsl:when test="./order-header/customer/shipping/title">
										<title>
											<xsl:value-of select="./order-header/customer/shipping/title"/>
										</title>
									</xsl:when>
									<xsl:otherwise>
										<title>Mr/Mrs</title>
									</xsl:otherwise>
								</xsl:choose>
								<firstName>
									<xsl:value-of select="substring-before(./order-header/customer/shipping/name, ' ')"/>
								</firstName>
								<lastName>
									<xsl:value-of select="substring-after(./order-header/customer/shipping/name, ' ')"/>
								</lastName>
								<company>
									<xsl:value-of select="./order-header/customer/shipping/company"/>
								</company>
								<division>
									<xsl:value-of select="./order-header/customer/shipping/division"/>
								</division>
								<addressLine1>
									<xsl:value-of select="./order-header/customer/shipping/street"/>
								</addressLine1>
								<town>
									<xsl:value-of select="./order-header/customer/shipping/street"/>
								</town>
								<country>UK</country>
								<postcode>
									<xsl:value-of select="./order-header/customer/shipping/zip"/>
								</postcode>
								<email>
									<xsl:value-of select="./order-header/customer/email"/>
								</email>
								<phone>
									<xsl:value-of select="./order-header/customer/shipping/phone"/>
								</phone>
							</address>
						</shippingInfo>
					</orderHeader>
					<orderItem>
						<xsl:for-each select="./order-detail/order-items/order-item">
							<itemDetail>
								<type>product</type>
								<lineId><xsl:value-of select="position()"/></lineId>
								<sku><xsl:value-of select="./item-sku"/></sku>
								<itemName><xsl:value-of select="./text"/></itemName>
								<itemDescription><xsl:value-of select="./text"/></itemDescription>
								<itemUrl><xsl:value-of select="concat('https://y5p.com/products/',./item-sku)"/></itemUrl>
								<grossPrice><xsl:value-of select="./unit-price"/></grossPrice>
								<netPrice><xsl:value-of select="./unit-price"/></netPrice>
								<discount>0.00</discount>
								<tax>0.5</tax>
								<currency>GBP</currency>
								<quantity><xsl:value-of select="./quantity"/></quantity>
								<itemStatus>
									<statusCode>I0001</statusCode>
									<status>pending</status>
									<isCancelled>false</isCancelled>
									<lastUpdatedTimestamp><xsl:value-of select="current-dateTime()"/></lastUpdatedTimestamp>
								</itemStatus>
							</itemDetail>
						</xsl:for-each>
					</orderItem>
				</orderData>
			</xsl:for-each>
		</OrderExtract>
	</xsl:template>
</xsl:stylesheet>
