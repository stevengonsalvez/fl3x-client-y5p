import groovy.json.JsonSlurper
import groovy.json.JsonOutput

def slurper = new JsonSlurper()

def jsonMap= slurper.parseText(payload)
def orderItem = jsonMap.orderData.orderItem.itemDetail
if (!(orderItem instanceof java.util.List)) {
def orderItemList = new java.util.ArrayList()
orderItemList.add(orderItem)
jsonMap.orderData.orderItem.itemDetail = orderItemList

}
payload = JsonOutput.toJson(jsonMap)